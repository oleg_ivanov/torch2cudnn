require 'cunn'
require 'opencv24'
--###### Module names table
--##    enum t7net_type
--##   {    0            1                       2        3       4          5                6                       7
--##    Sequential,ConcatTable,Conv2,Pool,Linear,Activation,SpatialContrast,Padding, \
--##       8      9               10            11                           12                    13
--##     View,Dropout,AddTable,SpatialBatchNorm,SpatialZeroPad,Identity
--##    };
--###########################

function serialize(module,fname)
   opencv24.push_var_binn_char(fname.."_name_",torch.type(module))
   print("@@@ PROCESSING MODULE WITH NAME:"..torch.type(module))
   local kW=module.kW or -1
   local kH=module.kH or -1
   local dW=module.dW or -1
   local dH=module.dH or -1
   local padW=module.padW or -1
   local padH=module.padH or -1
   local ichannels=module.nInputPlane or -1
   local ochannels=module.nOutputPlane or -1
   local lcn_thresh=module.threshold or -1
   local pool_type=-1
   local activation=-1
   local inp_size=-1
   local outp_size = -1
   local softmax_mode = -1
   local mod_type = -1


   if(torch.type(module)=='nn.Sequential') then
      mod_type = 0
      local modules = #module.modules
      print("SAVING SEQUENTAL MODULE, number of modules:",modules)
      opencv24.push_var_binn_int(fname.."_m_type",mod_type)
      opencv24.push_var_binn_int(fname.."_m_modules_size",modules)
      for i=1,modules do
	 print("### SAVING "..i.." MODULE")
	 serialize(module.modules[i],fname.."_sub_module_"..i)
      end
      return
   end

   if(torch.type(module)=='nn.ConcatTable') then
      mod_type = 1
      local modules = #module.modules
      print("SAVING CONCAT MODULE, number of modules:",modules)
      opencv24.push_var_binn_int(fname.."_m_type",mod_type)
      opencv24.push_var_binn_int(fname.."_m_modules_size",modules)
      for i=1,modules do
	 print("### SAVING "..i.." MODULE")
	 serialize(module.modules[i],fname.."_sub_module_"..i)
      end
      return
   end


   if(torch.type(module)=='nn.Linear') then
      inp_size = module.weight:size(2)
      outp_size = module.weight:size(1)
      mod_type = 4
   end


   if(torch.type(module)=='nn.SpatialMaxPooling') then
      pool_type =0
      mod_type =3
      padW=0
      padH=0
   end
   if(torch.type(module)=='nn.SpatialAveragePooling') then
      pool_type =1
      mod_type =3
      padW=0
      padH=0
   end

   if(torch.type(module) == 'nn.ReLU') then
      activation = 0
      mod_type = 5
   end

   if(torch.type(module)=='nn.SpatialConvolution') then
      mod_type = 2
   end


   if(torch.type(module)=='nn.SpatialContrastiveNormalization') then
      mod_type = 6
      module.weight = module.kernel
      opencv24.push_var_binn_int(fname.."_m_input_planes",module.nInputPlane)

      print("ninputplanes:",module.nInputPlane)
--      exit(-1)
   end

   if(torch.type(module)=='nn.View') then
      mod_type = 8
      inp_size = module.size[1]
      outp_size = module.size[1]
   end
   
   if(torch.type(module)=='nn.Dropout' or torch.type(module)=='nn.SpatialDropout') then
      mod_type = 9
   end

   if(torch.type(module)=='nn.Padding') then
      mod_type = 7
      ichannels = module.dim
      padW= module.pad
   end

   if(torch.type(module)=='nn.CAddTable') then
      mod_type = 10 
   end


   if(torch.type(module)=='nn.SpatialBatchNormalization') then
      mod_type = 11 
      local aff=1
      if(module.affine==false) then
              aff=0
      end
      opencv24.push_var_binn_int(fname.."_m_affine",aff)
      local running_std = torch.pow(module.running_var+module.eps,-1/2)
      opencv24.push_array_binn(fname.."_m_running_mean",module.running_mean:float():contiguous())
      opencv24.push_array_binn(fname.."_m_running_std",running_std:float():contiguous())
   end


   if(torch.type(module)=='nn.SpatialZeroPadding') then
      mod_type = 12 
      print("module:",module)
      opencv24.push_var_binn_int(fname.."_m_padL",module.pad_l)
      opencv24.push_var_binn_int(fname.."_m_padR",module.pad_r)
      opencv24.push_var_binn_int(fname.."_m_padT",module.pad_t)
      opencv24.push_var_binn_int(fname.."_m_padB",module.pad_b)

   end

   if(torch.type(module)=='nn.Identity') then
      mod_type = 13 
      print("module:",module)
   end



   opencv24.push_var_binn_int(fname.."_m_type",mod_type)


   opencv24.push_var_binn_int(fname.."_m_kW",kW)
   opencv24.push_var_binn_int(fname.."_m_kH",kH)
   opencv24.push_var_binn_int(fname.."_m_padW",padW)
   opencv24.push_var_binn_int(fname.."_m_padH",padH)
   opencv24.push_var_binn_int(fname.."_m_strideW",dW)
   opencv24.push_var_binn_int(fname.."_m_strideH",dH)

   opencv24.push_var_binn_int(fname.."_m_ichannels",ichannels)
   opencv24.push_var_binn_int(fname.."_m_ochannels",ochannels)

   opencv24.push_var_binn_float(fname.."_m_lcn_thresh",lcn_thresh)
   opencv24.push_var_binn_int(fname.."_m_pool_type",pool_type)

   opencv24.push_var_binn_int(fname.."_m_activation",activation)

   opencv24.push_var_binn_int(fname.."_m_inp_size",inp_size)
   opencv24.push_var_binn_int(fname.."_m_outp_size",outp_size)


   local weight=torch.rand(1,1,3,3):float():contiguous()
   if(module.weight) then
      weight = module.weight:float():contiguous() 
   end
   local bias = torch.rand(1,1,3,3):float():contiguous()
   if(module.bias) then
       bias = module.bias:float():contiguous() 
   end

   --p from dropout
   if(mod_type==9) then
      bias:fill(module.p)
   end

   opencv24.push_array_binn(fname.."_m_weight",weight)
   opencv24.push_array_binn(fname.."_m_bias",bias)
end
