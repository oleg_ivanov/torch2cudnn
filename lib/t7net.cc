#include "t7net.hh"
using namespace t7net;

cudnnHandle_t Tensor::m_CudnnHandle;
cublasHandle_t Tensor::m_CublasHandle;

cudnnTensorFormat_t  Tensor::m_tensorFormat;
cudnnDataType_t Tensor::m_dataType;
void t7net::deserialize(std::string fname,void* inet, binn* obj)
{

  t7net_container *net=(t7net_container*)inet;
  using namespace std;
  binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&(net->m_type));
  int mod_size;
  binn_object_get_int32(obj,(fname+string("_m_modules_size")).c_str(),&mod_size);
  printf("Sequential, got number of modules:%i\n",mod_size);
  net->modules.resize(mod_size);
  for(int i=0;i<mod_size;i++)
    {
      int m_num=i+1;
      int type;
      binn_object_get_int32(obj,(fname+string("_sub_module_")+utils::to_string(m_num)+string("_m_type")).c_str(),&type);
      printf("MODULE %i type:%i\n",i,type);

      if(1)
	{
	  //sequential
	  switch(type)
	    {
	    case t7net_base::Sequential:
	      net->modules[i]=new t7net_sequential();
	      break;
	    case t7net_base::ConcatTable:
	      net->modules[i]=new t7net_concat_table();
	      break;

	    case t7net_base::Conv2:
	      net->modules[i]=new t7net_Conv2d();
	      break;
	    case t7net_base::Pool:
	      net->modules[i]=new t7net_pool();
	      break;
	    case t7net_base::Linear:
	      net->modules[i]=new t7net_linear();
	      break;
	    case t7net_base::Activation:
	      net->modules[i]=new t7net_activation();
	      break;
	    case t7net_base::Padding:
	      net->modules[i]=new t7net_padding();
	      break;
	    case t7net_base::View:
	      net->modules[i]=new t7net_view();
	      break;
	    case t7net_base::Dropout:
	      net->modules[i]=new t7net_dropout();
	      break;
	    case t7net_base::AddTable:
	      net->modules[i]=new t7net_addtable();
	      break;
	    case t7net_base::SpatialBatchNorm:
	      net->modules[i]=new t7net_spatialbatchnorm();
	      break;
	    case t7net_base::SpatialZeroPad:
	      net->modules[i]=new t7net_spatialzeropad();
	      break;
	    case t7net_base::Identity:
	      net->modules[i]=new t7net_identity();
	      break;
	    case t7net_base::SpatialContrast:
	      net->modules[i]=new t7net_spatialcontrast();
	      break;


	    default:
	      printf("unknown module type %i!\n",type);
	      exit(-1);
	    }
	  string tmp=fname+string("_sub_module_")+utils::to_string(m_num);
	  printf("search string:%s\n",tmp.c_str());
	  printf("module:%p\n",net->modules[i]);
	  void *vp=(void*)net->modules[i];
	  switch(type)
	    {
	    case t7net_base::Sequential:
	      deserialize(tmp,vp,obj);
	      break;
	    case t7net_base::ConcatTable:
	      deserialize(tmp,vp,obj);
	      break;

	    default:
	      net->modules[i]->deserialize(tmp,obj);
	      break;

	    };
	}
    }
};

