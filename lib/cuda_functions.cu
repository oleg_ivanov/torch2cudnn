#include <cuda.h>
#include<cuda_runtime.h>
#include "cuda_functions.h"
#include <thrust/execution_policy.h>
#include "error_util.h"


  __device__ void print_tensor(int n, int c, int h, int w, float *data)
  {
};

struct sqr_op
{
  __device__ __host__ float operator()(const float x)
  {
    return x*x;
  };
};

struct sqrt_op
{
  __device__ __host__ float operator()(const float x)
  {
    return sqrt(x);
  };
};

struct threshold_op
{
  float A;
  threshold_op(float A):A(A)
  {
  }
  __device__ __host__ float operator()(const float x)
  {
    if(x>A)
      return x;
    return A;
  };
};

struct multiply_op
{
  float A;
  multiply_op(float A):A(A)
  {
  }
  __device__ __host__ float operator()(const float x)
  {
      return x*A;
  };
};

struct add_op
{
  float A;
  add_op(float A):A(A)
  {
  }
  __device__ __host__ float operator()(const float x)
  {
      return x+A;
  };
};

struct divide_op
{
  __device__ __host__ float operator()(const float x,const float y)
  {
      return x/y;
  };
};


struct subtract_op
{
  __device__ __host__ float operator()(const float x,const float y)
  {
      return x-y;
  };
};






void cuda_functions::apply_transform(float *data, uint amount,cuda_functions::functions mode)
{

  thrust::device_ptr<float> p1(data);
  switch(mode)
    {
    case cuda_functions::sqr:
      {
      sqr_op op;
      thrust::transform(thrust::device,p1,p1+amount,p1,op);
      }
      break;
    case cuda_functions::sqrt:
      {
      sqrt_op op;
      thrust::transform(thrust::device,p1,p1+amount,p1,op);
      }
      break;
    }

};

void cuda_functions::apply_transform(float *data, uint amount,cuda_functions::functions mode,float param)
{

  thrust::device_ptr<float> p1(data);
  switch(mode)
    {
    case cuda_functions::sqr:
      {
	sqr_op op;
	thrust::transform(thrust::device,p1,p1+amount,p1,op);
      }
      break;
    case cuda_functions::sqrt:
      {
      sqrt_op op;
      thrust::transform(thrust::device,p1,p1+amount,p1,op);
      }
      break;
    case cuda_functions::threshold:
      {
      threshold_op op(param);
      thrust::transform(thrust::device,p1,p1+amount,p1,op);
      }
      break;
    case cuda_functions::multiply:
      {
      multiply_op op(param);
      thrust::transform(thrust::device,p1,p1+amount,p1,op);
      }
    break;
    case cuda_functions::add:
      {
      add_op op(param);
      thrust::transform(thrust::device,p1,p1+amount,p1,op);
      }
    break;

    }

};

//data2 = data/data2
void cuda_functions::divide(float*data,float *data2, uint amount)
{
  divide_op op;
  thrust::transform(thrust::device_ptr<float>(data),thrust::device_ptr<float>(data)+amount,thrust::device_ptr<float>(data2),thrust::device_ptr<float>(data2),op);

}

void cuda_functions::subtract(float *data,float *data2, uint amount)
{
  subtract_op op;
  thrust::transform(thrust::device_ptr<float>(data),thrust::device_ptr<float>(data)+amount,thrust::device_ptr<float>(data2),thrust::device_ptr<float>(data2),op);

}



float cuda_functions::sum(float *data, uint amount)
{
  float rez=thrust::reduce(thrust::device_ptr<float>(data),thrust::device_ptr<float>(data)+amount, 0.f, thrust::plus<float>());
  return rez;
}






struct tsize
{
  int n1,n2,c1,c2,h1,h2,w1,w2;
};
//assuming we have nchw layout
__global__ void copy_kernel(float *dst,float *src,tsize dst_view, tsize dst_t, tsize src_view, tsize src_t)
{

  //coords inside view feature map
  const int xIndex = blockIdx.x * blockDim.x + threadIdx.x;
  const int yIndex = blockIdx.y * blockDim.y + threadIdx.y;
  const int cIndex = blockIdx.z;
  const int nIndex = 0;

  //  printf("CYX:%i %i %i block dim:%i %i %i grid dim:%i %i %i\n",cIndex,yIndex,xIndex,blockDim.x,blockDim.y,blockDim.z,gridDim.x,gridDim.y,gridDim.z);

  //coords in dst tensor
  const int dst_w = xIndex +  dst_view.w1;
  const int dst_h = yIndex +  dst_view.h1;
  const int dst_c = cIndex+dst_view.c1;
  const int dst_n =0;

  if(dst_c>dst_view.c2-1 or dst_h>dst_view.h2-1 or dst_w>dst_view.w2-1)
    return;



  //coords in src tensor
  const int src_w = xIndex +  src_view.w1;
  const int src_h = yIndex +  src_view.h1;
  const int src_c = cIndex+src_view.c1;
  const int src_n =0;

  const int dst_width = dst_view.w2-dst_view.w1;
  const int dst_height = dst_view.h2-dst_view.h1;
  const int dst_maps = dst_view.c2-dst_view.c1;

    const int src_width = src_view.w2-src_view.w1;
  const int src_height = src_view.h2-src_view.h1;
  const int src_maps = src_view.c2-src_view.c1;

  
 
  for(int n=dst_view.n1;n<dst_view.n2;n++)
    {
      //      const int dst_index = n*  (dst_width*dst_height*dst_maps) + dst_c* (dst_width*dst_height)+dst_h*dst_width + dst_w;

      const int dst_index = n*(dst_t.c2*dst_t.h2*dst_t.w2)+dst_c*(dst_t.w2*dst_t.h2)+dst_h*dst_t.w2+dst_w;
      
      const int n2= n-dst_view.n1 + src_view.n1;
      //      const int src_index = n2*  (src_width*src_height*src_maps) + src_c* (src_width*src_height)+src_h*src_width + src_w;
      const int src_index = n2*(src_t.c2*src_t.h2*src_t.w2) + src_c* (src_t.w2*src_t.h2)+src_h*src_t.w2+ src_w;


      //      printf("n=%i,bounds:%i %i, n2:%i, bounds:%i %i\n",n,coords[0],coords[1],n2,coords[0+8],coords[1+8]);

      //      printf("dst_t size:%i %i %i %i\n",dst_t.n2,dst_t.c2,dst_t.h2,dst_t.w2);
      //      printf("dst[%i %i %i %i (%i)] = %f\n",dst_n,dst_c,dst_h,dst_w,dst_index,dst[dst_index]);
      dst[dst_index] = src[src_index];

      //      printf("src[%i %i %i %i (%i)] = %f\n",src_n,src_c,src_h,src_w,src_index,src[src_index]);

    }


  
  
};
void cuda_functions::copy(float*dst,float *src,int *dst_view2,int*src_view2,int *dst_desc,int*src_desc)
{
  tsize dst_view,dst_t,src_view,src_t;
  //  printf("fast copy started\n");

  dst_view.n1 = dst_view2[0];
  dst_view.n2 = dst_view2[1];
 
  dst_view.c1 = dst_view2[2];
  dst_view.c2 = dst_view2[3];

  dst_view.h1 = dst_view2[4];
  dst_view.h2 = dst_view2[5];

  dst_view.w1 = dst_view2[6];
  dst_view.w2 = dst_view2[7];


  dst_t.n1 = dst_desc[0];
  dst_t.n2 = dst_desc[0];
 
  dst_t.c1 = dst_desc[1];
  dst_t.c2 = dst_desc[1];

  dst_t.h1 = dst_desc[2];
  dst_t.h2 = dst_desc[2];

  dst_t.w1 = dst_desc[3];
  dst_t.w2 = dst_desc[3];







  

  src_view.n1 = src_view2[0];
  src_view.n2 = src_view2[1];
 
  src_view.c1 = src_view2[2];
  src_view.c2 = src_view2[3];

  src_view.h1 = src_view2[4];
  src_view.h2 = src_view2[5];

  src_view.w1 = src_view2[6];
  src_view.w2 = src_view2[7];



  src_t.n1 = src_desc[0];
  src_t.n2 = src_desc[0];
 
  src_t.c1 = src_desc[1];
  src_t.c2 = src_desc[1];

  src_t.h1 = src_desc[2];
  src_t.h2 = src_desc[2];

  src_t.w1 = src_desc[3];
  src_t.w2 = src_desc[3];




  //TORCH, threads per block: 32*16dd

  int block_dx = min(dst_view.w2-dst_view.w1,16);
  int block_dy = min(dst_view.h2-dst_view.h1,16);

  
  const dim3 block(block_dx,block_dy);
  //  printf("test:%f %f \n",std::ceil(1.3),std::floor(1.3));


  int c2_diff = dst_view.c2 - dst_view.c1;
  float grids= min(c2_diff,36);
  
  //  grids  = 36*4;
  const dim3 grid(std::ceil((dst_view.w2-dst_view.w1)/float(block.x))+1,std::ceil((dst_view.h2-dst_view.h1)/float(block.y))+1,std::ceil((dst_view.c2-dst_view.c1)));
  //  printf("grid:%i %i %i\n",grid.x,grid.y,grid.z);


  
  copy_kernel<<<grid,block>>>(dst,src,dst_view,dst_t,src_view,src_t);
}






//@@@@@@@@@@   SpatialUpsamplingNearest   @@@@@@@@@@@@@@
__global__ void upsampling_kernel(float *dst,float *src,tsize dst_view, tsize dst_t, tsize src_view, tsize src_t)
{

  //coords inside view feature map
  const int xIndex = blockIdx.x * blockDim.x + threadIdx.x;
  const int yIndex = blockIdx.y * blockDim.y + threadIdx.y;
  const int cIndex = blockIdx.z;
  const int nIndex = 0;

  //  printf("CYX:%i %i %i block dim:%i %i %i grid dim:%i %i %i\n",cIndex,yIndex,xIndex,blockDim.x,blockDim.y,blockDim.z,gridDim.x,gridDim.y,gridDim.z);

  //coords in dst tensor
  const int dst_w = xIndex +  dst_view.w1;
  const int dst_h = yIndex +  dst_view.h1;
  const int dst_c = cIndex+dst_view.c1;
  const int dst_n =0;




  //coords in src tensor
  const int src_w = xIndex +  src_view.w1;
  const int src_h = yIndex +  src_view.h1;
  const int src_c = cIndex+src_view.c1;
  const int src_n =0;


  if(src_c>src_view.c2-1 or src_h>src_view.h2-1 or src_w>src_view.w2-1)
    return;


  const int dst_width = dst_view.w2-dst_view.w1;
  const int dst_height = dst_view.h2-dst_view.h1;
  const int dst_maps = dst_view.c2-dst_view.c1;


    const int src_width = src_view.w2-src_view.w1;
  const int src_height = src_view.h2-src_view.h1;
  const int src_maps = src_view.c2-src_view.c1;

  const int coef=dst_width/src_width;

  const int dst_maxI=dst_t.n2*dst_t.c2*dst_t.h2*dst_t.w2;
  
 
  for(int n=dst_view.n1;n<dst_view.n2;n++)
    {
      const int n2= n-dst_view.n1 + src_view.n1;
      //      const int src_index = n2*  (src_width*src_height*src_maps) + src_c* (src_width*src_height)+src_h*src_width + src_w;
      const int src_index = n2*(src_t.c2*src_t.h2*src_t.w2) + src_c* (src_t.w2*src_t.h2)+src_h*src_t.w2+ src_w;

      for(int y=0;y<coef;y++)
	for(int x=0;x<coef;x++)
	  {

	    //	    const int dst_index = n*(dst_t.c2*dst_t.h2*dst_t.w2)+dst_c*(dst_t.w2*dst_t.h2)+dst_h*dst_t.w2+dst_w;
	    const int dst_index = n*(dst_t.c2*dst_t.h2*dst_t.w2)+dst_c*(dst_t.w2*dst_t.h2)+(dst_h*coef+y)*dst_t.w2+(dst_w*coef+x);

	    /*
	    if(dst_index>dst_maxI)
	      {
		printf("ACHTUNG! out of  bound!\n");
		printf("max:%i %i %i %i actual:%i %i %i %i coef:%i\n",dst_t.n2,dst_t.c2,dst_t.h2,dst_t.w2,n,dst_c,dst_h*coef+y,dst_w*coef+x,coef);
		
	      }
	    */

	    //      printf("n=%i,bounds:%i %i, n2:%i, bounds:%i %i\n",n,coords[0],coords[1],n2,coords[0+8],coords[1+8]);

	    //      printf("dst_t size:%i %i %i %i\n",dst_t.n2,dst_t.c2,dst_t.h2,dst_t.w2);
	    //      printf("dst[%i %i %i %i (%i)] = %f\n",dst_n,dst_c,dst_h,dst_w,dst_index,dst[dst_index]);
	    dst[dst_index] = src[src_index];
	  }

      //      printf("src[%i %i %i %i (%i)] = %f\n",src_n,src_c,src_h,src_w,src_index,src[src_index]);

    }


  
  
};


//assuming we already have doubled size of dst 
void cuda_functions::spatial_upsampling_nearest(float*dst,float *src,int *dst_view2,int*src_view2,int *dst_desc,int*src_desc)
{
  tsize dst_view,dst_t,src_view,src_t;
  //  printf("fast copy started\n");

  dst_view.n1 = dst_view2[0];
  dst_view.n2 = dst_view2[1];
 
  dst_view.c1 = dst_view2[2];
  dst_view.c2 = dst_view2[3];

  dst_view.h1 = dst_view2[4];
  dst_view.h2 = dst_view2[5];

  dst_view.w1 = dst_view2[6];
  dst_view.w2 = dst_view2[7];


  dst_t.n1 = dst_desc[0];
  dst_t.n2 = dst_desc[0];
 
  dst_t.c1 = dst_desc[1];
  dst_t.c2 = dst_desc[1];

  dst_t.h1 = dst_desc[2];
  dst_t.h2 = dst_desc[2];

  dst_t.w1 = dst_desc[3];
  dst_t.w2 = dst_desc[3];







  

  src_view.n1 = src_view2[0];
  src_view.n2 = src_view2[1];
 
  src_view.c1 = src_view2[2];
  src_view.c2 = src_view2[3];

  src_view.h1 = src_view2[4];
  src_view.h2 = src_view2[5];

  src_view.w1 = src_view2[6];
  src_view.w2 = src_view2[7];



  src_t.n1 = src_desc[0];
  src_t.n2 = src_desc[0];
 
  src_t.c1 = src_desc[1];
  src_t.c2 = src_desc[1];

  src_t.h1 = src_desc[2];
  src_t.h2 = src_desc[2];

  src_t.w1 = src_desc[3];
  src_t.w2 = src_desc[3];




  //TORCH, threads per block: 32*16dd

  int block_dx = min(src_view.w2-src_view.w1,16);
  int block_dy = min(src_view.h2-src_view.h1,16);

  
  const dim3 block(block_dx,block_dy);
  //  printf("test:%f %f \n",std::ceil(1.3),std::floor(1.3));


  int c2_diff = dst_view.c2 - dst_view.c1;
  float grids= min(c2_diff,36);
  
  //  grids  = 36*4;
  const dim3 grid(std::ceil((dst_view.w2-dst_view.w1)/float(block.x))+1,std::ceil((dst_view.h2-dst_view.h1)/float(block.y))+1,std::ceil((dst_view.c2-dst_view.c1)));
  //  printf("grid:%i %i %i\n",grid.x,grid.y,grid.z);


  
  upsampling_kernel<<<grid,block>>>(dst,src,dst_view,dst_t,src_view,src_t);
}




