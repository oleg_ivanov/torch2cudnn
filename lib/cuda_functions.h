#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>


struct cuda_functions
{
  enum  functions{sqr,sqrt,log,threshold,multiply,add};
  int type;
  void print_tensor(int n, int c, int h, int w, float *data);
  static  void apply_transform(float *data, uint amount,functions mode);
  static  void apply_transform(float *data, uint amount,functions mode,float param);
  static void divide(float *, float *, uint amount);
  static void subtract(float *, float *, uint amount);
  static float sum(float *, uint amount);

  static void copy(float*dst,float *src,int *dst_view,int*src_view,int* dst_lay,int* src_lay);
  static void spatial_upsampling_nearest(float*dst,float *src,int *dst_view,int*src_view,int* dst_lay,int* src_lay);
  static void spatial_maxout(float*dst,float *src,int *dst_view,int*src_view,int* dst_lay,int* src_lay);

};
