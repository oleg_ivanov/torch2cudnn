#ifndef t7net_
#define t7net_


#include <vector>
#include <cv.h>
#include <highgui.h>

#include <stdint.h>
#include <cuda.h>
#include "error_util.h"
#include <cublas_v2.h>
#include <cudnn.h>
#include "cuda_functions.h"
#include <map>

#include "binn/binn.hh"
#include <fstream>
#include <iostream>
#include <omp.h>

typedef unsigned char uchar;

namespace t7net
{
  struct utils
  {
    static std::string to_string(int i)
    {
      char l[200];
      sprintf(l,"%i",i);
      //      printf("l:!%s!\n",l);
      return std::string(l);
    }
    static std::string to_string(float i)
    {
      char l[200];
      sprintf(l,"%f",i);
      return std::string(l);
    }

    static void load_binn(std::string fname, binn *obj)
    {
      std::ifstream out(fname.c_str(),std::ios::binary|std::ios::ate);
      long size=out.tellg();
      out.seekg(0,std::ios:: beg);
      void *pointer=new uchar[size];
      out.read(reinterpret_cast<char*>(pointer),size);
      out.close();
      binn_load(pointer,obj);
    }
  };

  struct Tensor_view
  {
    //tensor dimensions
    int n1,n2;
    int c1,c2;
    int h1,h2;
    int w1,w2;
    Tensor_view(int in1,int in2,int ic1,int ic2,int ih1,int ih2,int iw1,int iw2):n1(in1),n2(in2),c1(ic1),c2(ic2),h1(ih1),h2(ih2),w1(iw1),w2(iw2)
    {
    }
    void print()
    {
      printf("view: %i<-%i %i<-%i %i<-%i %i<-%i\n",n1,n2,c1,c2,h1,h2,w1,w2);
    }
    static bool compare_dimensions(Tensor_view v1, Tensor_view v2)
    {
      if((v1.n2-v1.n1)!=(v2.n2-v2.n1))
	return false;
      if((v1.c2-v1.c1)!=(v2.c2-v2.c1))
	return false;
      if((v1.h2-v1.h1)!=(v2.h2-v2.h1))
	return false;
      if((v1.w2-v1.w1)!=(v2.w2-v2.w1))
	return false;
      return true;
    }
  };


  struct Tensor
  {
    float *data;
    cudnnTensorDescriptor_t desc;
    int n,c,h,w;

    int on_device;

    int *m_refcount;
    

    static cudnnTensorFormat_t  m_tensorFormat;
    static cudnnDataType_t m_dataType;

    
    static cudnnHandle_t m_CudnnHandle;
    static cublasHandle_t m_CublasHandle;


    
    static void init_handles(int device)
    {

      checkCudaErrors( cudaSetDevice(device) );
     
      m_tensorFormat = CUDNN_TENSOR_NCHW;
      m_dataType =  CUDNN_DATA_FLOAT;

      checkCUDNN( cudnnCreate(&m_CudnnHandle) );
      checkCublasErrors( cublasCreate(&m_CublasHandle) );
    }
    static bool compare_dimensions(Tensor v1, Tensor v2)
    {
      if(v1.n!=v2.n)
	return false;
      if(v1.c!=v2.c)
	return false;
      if(v1.h!=v2.h)
	return false;
      if(v1.w!=v2.w)
	return false;
      return true;
    }

    static void destroy_handles()
    {
      checkCUDNN( cudnnDestroy(m_CudnnHandle) );
      checkCublasErrors( cublasDestroy(m_CublasHandle) );
    }

    Tensor():data(NULL),desc(NULL),n(-1),c(-1),h(-1),w(-1)
    {
      on_device = 1;
      m_refcount=NULL;
    };


    Tensor(int in, int ic, int ih, int iw)
    {
      alloc_tensor_4d(*this,in,ic,ih,iw);
    }

    void addref()
    {
      if(m_refcount)
      CV_XADD(m_refcount,1);
    }
    Tensor & operator = (const Tensor &m)
    {
      copy_constructor(*this,m);
    }
    Tensor (const Tensor &other)
    {
      m_refcount=NULL;
      data=NULL;
      copy_constructor(*this,other);
    }
    void copy_constructor(Tensor &m2,const Tensor &m)
    {
      if(&m2!=&m)
	{
	  if(m2.m_refcount)
	    m2.release();
	  if( m.m_refcount )
	    CV_XADD(m.m_refcount,1);
	  //            m.m_refcount += 1;                                                                                   
	  m2.m_refcount = m.m_refcount;
	  m2.data=m.data;
	  m2.desc=m.desc;
	  m2.n=m.n;
	  m2.c=m.c;
	  m2.h=m.h;
	  m2.w=m.w;
	  m2.on_device=m.on_device;
	}
    }

   
    Tensor_view view()
    {
      return Tensor_view(0,n,0,c,0,h,0,w);
    }
    
    static    void print_mat(cv::Mat &m)
    {
      int cols=8;
      for(int i=0;i<m.channels();i++)
	{
	  for(int x=0;x<m.cols;x+=cols)
	    {
	      printf("\nColumns %i to %i\n",x,std::min(x+cols,int(m.cols)));
	      for(int y=0;y<m.rows;y++)
		{
		  for(int x1=x;x1<std::min(x+cols,m.cols);x1++)
		    {
		      float*p=(float*)m.data;
		      printf("%.4f ",p[m.cols*m.rows*i+y*m.cols+x1]);
		    }
		  printf("\n");
		}
	    }
	}
    }

    void    print()
    {
      if(n<0)
	{
	  printf("Null tensor\n");
	  exit(-1);
	}
      int n,c,h,w;
      tensor_size(*this,n,c,h,w);
      printf("tensor size:%ix%ix%ix%i, data pointer:%p desc pointer:%p,sum:%f m_refcount:%p\n",n,c,h,w,data,desc,Tensor::sum(*this),m_refcount);
      for(int i=0;i<n;i++)
	{
	  printf("Batch:%i\n",i);
	  cv::Mat tmp;
	  for(int j=0;j<c;j++)
	    {
	      mat_from_tensor(tmp,*this,i,j);
	      printf("FMap:%i\n",j);
	      print_mat(tmp);
	    }
	}
    }

    void print(Tensor_view &center)
    {
      Tensor out2(center.n2-center.n1,center.c2-center.c1,center.h2-center.h1,center.w2-center.w1);
      Tensor_view out2_view=out2.view();
      Tensor::copy(out2_view,out2,center,*this);
      out2.print();
      out2.release();
    }
    void print_geom()
    {
      int n,c,h,w;
      tensor_size(*this,n,c,h,w);
      if(m_refcount)
	printf("tensor size:%ix%ix%ix%i, data pointer:%p desc pointer:%p on device:%i m_recount:%p %i sum:%f\n",n,c,h,w,data,desc,on_device,m_refcount,*m_refcount,sum(*this));
      else
		printf("tensor size:%ix%ix%ix%i, data pointer:%p desc pointer:%p on device:%i m_recount:%p\n",n,c,h,w,data,desc,on_device,m_refcount);

    }


    static void alloc_tensor_4d(Tensor &tmp,int n, int c, int h, int w)
    {
      //      Tensor tmp;
      tmp.on_device=1;
      tmp.n=n;
      tmp.c=c;
      tmp.h=h;
      tmp.w=w;
      checkCUDNN( cudnnCreateTensorDescriptor(&tmp.desc) );
      checkCUDNN( cudnnSetTensor4dDescriptor(tmp.desc, m_tensorFormat, CUDNN_DATA_FLOAT, n, c, h, w) );
      int size_b = n*c*h*w*sizeof(float);
      checkCudaErrors( cudaMalloc((void**)&tmp.data, size_b) );
      tmp.m_refcount=new int;
      *tmp.m_refcount=1;
      //      CV_XADD(tmp.m_refcount,1);
      //      printf("ALLOCATION OF TENSOR:data=%p refcount=%p\n",tmp.data,tmp.m_refcount);
      //      return tmp; 
    }
    /*
    static Tensor alloc_tensor_3d(int c, int h, int w)
    {
      return alloc_tensor_4d(1,c,h,w);
    }
    static Tensor alloc_tensor_2d(int h, int w)
    {
      return alloc_tensor_4d(1,1,h,w);
    }
    */

    static    void tensor_size(Tensor&t,int &n, int &c, int &h, int &w)
    {
      cudnnDataType_t datatype;
      int ns,cs,hs,ws;
      checkCUDNN( cudnnGetTensor4dDescriptor(t.desc,&datatype,&n,&c,&h,&w,&ns,&cs,&hs,&ws) );
    }

    static void to_tensor(void* data_h,size_t size_b, float*t_data)
    {
      checkCudaErrors( cudaMemcpy((void*)t_data, data_h,size_b,cudaMemcpyHostToDevice) );
    }

    static Tensor from_color_mat(cv::Mat &m)
    {
      using namespace cv;
      Tensor tmp = Tensor(1,m.channels(),m.rows,m.cols);
      tmp.fill(0);
      Mat s[3],s2[3];
      if(m.channels()==3)
	{
	  split(m,s);
	  s2[0]=s[2];
	  s2[1]=s[1];
	  s2[2]=s[0];
	  for(int i=0;i<3;i++)
	    {
	      s2[i].convertTo(s2[i],CV_32F);
	      s2[i]/=256.;
	      mat_to_tensor(s2[i],tmp,0,i);
	    }
	}
      else
	{
	  Mat tmp2;
	  m.convertTo(tmp2,CV_32F);
	  tmp2/=256.;
	  mat_to_tensor(tmp2,tmp,0,0);
	}
      return tmp;
    }
    static void mat_from_tensor(cv::Mat &m,Tensor&t,int dim)
    {
      int n,c,h,w;
      tensor_size(t,n,c,h,w);
      m=cv::Mat(h,w,CV_32FC3);

      float *data_h=(float*)m.data;
      int size_b=m.cols*m.rows*m.channels()*sizeof(float);
      //      printf("sizeb:%i\n",size_b);
      checkCudaErrors( cudaMemcpy(data_h,t.data+m.cols*m.rows*m.channels()*(dim),size_b,cudaMemcpyDeviceToHost) );
    }
    static void mat_from_tensor(cv::Mat &m,Tensor&t,int i_n,int i_c)
    {
      int n,c,h,w;
      tensor_size(t,n,c,h,w);
      m=cv::Mat(h,w,CV_32FC1);
      float *data_h=(float*)m.data;
      int size_b=h*w*sizeof(float);
      //      printf("sizeb:%i\n",size_b);
      checkCudaErrors( cudaMemcpy(data_h,t.ptr(i_n,i_c),size_b,cudaMemcpyDeviceToHost));

    }
    //assuming we always have 4d tensor
    static void mat_to_tensor(cv::Mat &m, Tensor &t, int dim)
    {
      int n,c,h,w;
      tensor_size(t,n,c,h,w);

      assert(c==m.channels());
      to_tensor((void*)m.data,m.rows*m.cols*m.channels()*sizeof(float),t.data+m.cols*m.rows*m.channels()*(dim)); 
      
    }
    static void mat_to_tensor(cv::Mat &m,Tensor&t,int i_n,int i_c)
    {
      int n,c,h,w;
      tensor_size(t,n,c,h,w);
      assert(m.cols==w&&m.rows==h);
      float *data_h=(float*)m.data;
      int size_b=h*w*sizeof(float);
      //      printf("sizeb:%i\n",size_b);
      checkCudaErrors( cudaMemcpy(t.data+c*h*w*(i_n)+h*w*i_c,data_h,size_b,cudaMemcpyHostToDevice));

    }


    static void from_tensor(float* &data_h, Tensor&t)
    {
      int n,c,h,w;
      tensor_size(t,n,c,h,w);
      size_t size_b=n*c*h*w*sizeof(float);
      checkCudaErrors( cudaMemcpy(t.data, data_h,size_b,cudaMemcpyDeviceToHost) );

    }



    static void tensor_to_tensor(float *src, float *dst, size_t amount)
    {
      checkCudaErrors( cudaMemcpy(dst, src, amount, cudaMemcpyDeviceToDevice) );
      //      checkCudaErrors( cudaMemcpyAsync(dst, src, amount, cudaMemcpyDeviceToDevice) );

    }

    static void copy_fast(Tensor_view &dst, Tensor&dst_t, Tensor_view &src,Tensor &src_t)
    {
      if(dst_t.on_device&&src_t.on_device)
	{
	  int dst_view[]={dst.n1,dst.n2,dst.c1,dst.c2,dst.h1,dst.h2,dst.w1,dst.w2};
	  int src_view[]={src.n1,src.n2,src.c1,src.c2,src.h1,src.h2,src.w1,src.w2};
	  int dst_lay[]={dst_t.n,dst_t.c,dst_t.h,dst_t.w};
	  int src_lay[]={src_t.n,src_t.c,src_t.h,src_t.w};

	  cuda_functions::copy(dst_t.data,src_t.data,dst_view,src_view,dst_lay,src_lay); 

	  
	  //	  exit(-1);
	  return;
	};
     
    }
      
    static void copy(Tensor &dst,Tensor &src)
    {
      if(0)
	{
	  printf("copying\n");
	  printf("dst:\n");
	  dst.print_geom();
	  printf("src:\n");
	  src.print_geom();
	}
      copy(dst.view(),dst,src.view(),src);
    }

    static    void copy(Tensor_view dst, Tensor&dst_t, Tensor_view src,Tensor &src_t)
    {


      assert(dst_t.m_refcount);
      assert(*dst_t.m_refcount>0);

      assert(src_t.m_refcount);
      assert(*src_t.m_refcount>0);

      
      assert((dst.n2-dst.n1)==(src.n2-src.n1)&&(dst.c2-dst.c1)==(src.c2-src.c1)&&(dst.h2-dst.h1)==(src.h2-src.h1)&&(dst.w2-dst.w1)==(src.w2-src.w1));


      //      printf("copying tensor of size:%i %i %i %i\n",dst.n2-dst.n1,dst.c2-dst.c1,dst.h2-dst.h1,dst.w2-dst.w1);
      int fast_flag=1;

      if(dst.w2-dst.w1>500 or dst.h2-dst.h1 > 500)
	fast_flag=0;

      
      if(dst_t.on_device&&src_t.on_device&&fast_flag)
	{
	  copy_fast(dst,dst_t,src,src_t);
	  return;
	};
      
      for(int i1=src.n1;i1<src.n2;i1++)
	//#pragma omp parallel for
	for(int i2=src.c1;i2<src.c2;i2++)
	  for(int i3=src.h1;i3<src.h2;i3++)
	    {
	      float *src_p = src_t.ptr(i1,i2,i3)+src.w1;
	      float *dst_p = dst_t.ptr(i1-src.n1+dst.n1,i2-src.c1+dst.c1,i3-src.h1+dst.h1)+dst.w1; 
	      if(dst_t.on_device ==0 &&src_t.on_device==0)
		{
		  memcpy(dst_p,src_p,(src.w2-src.w1)*sizeof(float));
		}
	      else
		tensor_to_tensor(src_p,dst_p,(src.w2-src.w1)*sizeof(float));
	    }
    };


    

    float *ptr(int in)
    {
      return data+in*(c*h*w);
    }
    float *ptr(int in,int ic)
    {
      return data+in*(c*h*w)+ic*(h*w);
    }
    float *ptr(int in, int ic, int ih)
    {
      return data+in*(c*h*w)+ic*(h*w)+ih*w;
    }
    float *ptr(int in, int ic, int ih,int iw)
    {
      return data+in*(c*h*w)+ic*(h*w)+ih*w+iw; 
    }


    void set(int in,int ic,int ih,int iw,float v)
    {
      float value=v;
      to_tensor((void*)&value,sizeof(float),data+in*(c*h*w)+ic*(h*w)+ih*w+iw);
    }

    void set(int in,int ic,int ih,int iw, Tensor_view &view, float v)
    {
      float value=v;
      to_tensor((void*)&value,sizeof(float),data+(in+view.n1)*(c*h*w)+(ic+view.c1)*(h*w)+(ih+view.h1)*w+iw+view.w1);
    }

    float get(int in,int ic,int ih,int iw)
    {
      if(on_device==0)
	return ptr(in,ic,ih,iw)[0];
      else
	{
	  printf("achtung! tensor is on device!\n");
	}
    }
    float get(int in,int ic,int ih,int iw,Tensor_view &view)
    {
      if(on_device==0)
	return ptr(in+view.n1,ic+view.c1,ih+view.h1,iw+view.w1)[0];
      else
	{
	  printf("achtung! tensor is on device!\n");
	}
    }
    void to_host_tensor(Tensor &t)
    {

      t.release();
      
      t.data = new float[n*c*h*w];
      size_t size_b = n*c*h*w*sizeof(float);
      checkCudaErrors( cudaMemcpy(t.data,this->data,size_b,cudaMemcpyDeviceToHost));

      t.n=n;
      t.c=c;
      t.h=h;
      t.w=w;
      t.on_device = 0;
      t.m_refcount=new int;
      *t.m_refcount=1;
    }

    

    void fill(float value)
    {
      float v[1];
      v[0]=value;
      checkCUDNN(cudnnSetTensor(m_CudnnHandle,desc,data,v));
    }

    void rand_fill()
    {
      cv::RNG rng;
      for(int in=0;in<n;in++)
	for(int ic=0;ic<c;ic++)
	  for(int ih=0;ih<h;ih++)
	    for(int iw=0;iw<w;iw++)
	      this->set(in,ic,ih,iw,rng.uniform(0,1)); 
    }

    Tensor clone()
    {
      Tensor tmp=Tensor(n,c,h,w);
      tensor_to_tensor(data,tmp.data,n*c*h*w*sizeof(float));
      //      tmp.print();
      return tmp;
    }

    void reshape(int in, int ic, int ih, int iw)
    {
      if(in*ic*ih*iw!=n*c*h*w)
	{
	  printf("unable to reshape tensor! bailing out!\n");
	  printf("my size:%i %i %i %i expected:%i %i %i %i\n",n,c,h,w,in,ic,ih,iw);
	  exit(-1);
	}
      n=in;
      c=ic;
      h=ih;
      w=iw;

      checkCUDNN( cudnnSetTensor4dDescriptor(desc,m_tensorFormat,CUDNN_DATA_FLOAT,n,c,h,w) );
    }


    
 
    void release()
    {
      if(on_device &&m_refcount)
	{
	  if(m_refcount && CV_XADD(m_refcount, -1) == 1 )
	    {
	      if(data)
		cudaFree(data);
	      if(desc)
		{
		  cudnnDestroyTensorDescriptor(desc);
		}
	    }
	};
      if(on_device==0&&m_refcount)
	{
	  if(m_refcount && CV_XADD(m_refcount, -1) == 1 )
	    {
	      if(data)
		delete data;
	    };
	}
      data=NULL;
      desc=NULL;
      m_refcount=NULL;
    }


    

    ~Tensor()
    {
      release();
      data=NULL;
      desc=NULL;
      m_refcount=NULL;
    }

    static    void apply_transform(Tensor &t, cuda_functions::functions mode)
    {
      cuda_functions::apply_transform(t.data,t.n*t.c*t.h*t.w,mode);
    }

    static    void apply_transform(Tensor &t, cuda_functions::functions mode, float param)
    {
      cuda_functions::apply_transform(t.data,t.n*t.c*t.h*t.w,mode,param);
    }
    //divide one tensor by another, keeping result in second tensor
    static void divide(Tensor &t1, Tensor &t2)
    {
      cuda_functions::divide(t1.data,t2.data,t1.n*t1.c*t1.h*t1.w);
    }
    static void subtract(Tensor &t1, Tensor &t2)
    {
      cuda_functions::subtract(t1.data,t2.data,t1.n*t1.c*t1.h*t1.w);
    }

    static float sum(Tensor &t)
    {
      return cuda_functions::sum(t.data,t.n*t.c*t.h*t.w);
    }


    static void serialize(std::string fname, binn* obj, Tensor &t)
    {
      using namespace std;
      binn_object_set_int32(obj,(fname+string("_n")).c_str(),t.n);
      binn_object_set_int32(obj,(fname+string("_c")).c_str(),t.c);
      binn_object_set_int32(obj,(fname+string("_h")).c_str(),t.h);
      binn_object_set_int32(obj,(fname+string("_w")).c_str(),t.w);

      string s=(fname+string("_data"));

      float *ldata = new float[t.n*t.c*t.h*t.w];
      
      from_tensor(ldata,t);
      
      int rez=binn_object_set_blob(obj,s.c_str(),ldata,t.n*t.c*t.h*t.w*sizeof(float));
      delete [] ldata;
    }

    static void deserialize(std::string fname, binn* obj, Tensor &t)
    {
      using namespace std;

      t.release();
      
      int n,c,h,w;
      binn_object_get_int32(obj,(fname+string("_n")).c_str(),&n);
      binn_object_get_int32(obj,(fname+string("_c")).c_str(),&c);
      binn_object_get_int32(obj,(fname+string("_h")).c_str(),&h);
      binn_object_get_int32(obj,(fname+string("_w")).c_str(),&w);

      int size = n*c*h*w*sizeof(float);
      float *ldata = new float[n*c*h*w];
     
      string s=(fname+string("_data"));
      void*p;
      int rez=binn_object_get_blob(obj,s.c_str(),&p,&size);
      memcpy(ldata,p,size);

      t=Tensor(n,c,h,w);
      //      t=Tensor(n,c,h,w);
      to_tensor(ldata,size,t.data);
    }
static       void test(Tensor b)
      {
	printf("test\n");
	b.print_geom();
      }
   
    static void destructor_tests()
    {
      
      Tensor *a=new Tensor(1,1,4,4);
      a->fill(1);
      a->print();

      Tensor b;
      {
	//	Tensor a=Tensor(1,1,4,4);
	a->fill(1);
	a->print();
	b=*a;
      };
      delete a;
      printf("b=\n");
      b.print();
      printf("###############\n");

      test(b);
      test(b);
      test(b);
      test(b);

      
    };
  };


  //base for all network modules
  class t7net_base
  {
  public:
    enum t7net_type
      {
	Sequential,ConcatTable,Conv2,Pool,Linear,Activation,SpatialContrast,Padding, \
	View,Dropout,AddTable,SpatialBatchNorm,SpatialZeroPad,Identity,
      };

    std::vector<Tensor> m_input;
    std::vector<Tensor> m_output;
    //type of module
    int m_type;

    t7net_base()
    {
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_base()
    {
      for(int i=0;i<m_input.size();i++)
	m_input[i].release();
      for(int i=0;i<m_output.size();i++)
	m_output[i].release();
    }

    virtual int input_elements()
    {
      return 1;
    }
    virtual int output_elements()
    {
      return 1;
    }

    virtual void forward(int debug=0)
    {
      printf("you are trying to launch pure virutal forward function from t7net base module!\n");
      std::string s;
      print(s);
      exit(-1);
    }
    virtual void serialize(std::string fname, binn*obj)
    {
    }
    virtual void deserialize(std::string fname, binn*obj)
    {
    }

    virtual    void print(std:: string )
    {
    }
    virtual void print_weight()
    {
    }
  };


  //nn.Sequential 
  class t7net_container:public t7net_base
  {
  public:
    std::vector<t7net_base *> modules;
    virtual void add(t7net_base *mod,int pos)
    {
      modules[pos]=mod;
      if(pos==modules.size()-1)
	this->m_output = modules[modules.size()-1]->m_output;
      if(pos>0&&pos<modules.size()-1)
	{
	  modules[pos]->m_input = modules[pos-1]->m_output;
	}
    }
  };

  

  class t7net_sequential:public t7net_container
  {
    enum types
      {
        module_t,seq_t,tab_t
      };
  public:
    t7net_sequential()
    {
      m_input.resize(1);
      m_output.resize(1);
    }

    ~t7net_sequential()
    {
      for(int i=0;i<modules.size();i++)
	delete modules[i];
    }

    void add(t7net_base * mod)
    {
      modules.push_back(mod);
      this->m_output = modules[modules.size()-1]->m_output;
      if(modules.size()>1)
	{
	  t7net_base *p2 = (t7net_base*) modules[modules.size()-2];
	  modules[modules.size()-1]->m_input = modules[modules.size()-2]->m_output;
	  this->m_output= modules[modules.size()-1]->m_output;
	}
    }
    void add_(t7net_base * mod)
    {
      modules.push_back(mod);
    }

    void add(t7net_base * mod,int pos)
    {
      modules[pos]=mod;
      if(pos==modules.size()-1)
	this->m_output = modules[modules.size()-1]->m_output;
      if(pos>0&&pos<modules.size()-1)
	{
	  modules[pos]->m_input = modules[pos-1]->m_output;
	}
    }


    

    
    void forward(int debug=0)
    {
      if(modules.size()>0)
	{
	  modules[0]->m_input = this->m_input;
	  for(int i=0;i<modules.size();i++)
	    {
	      if(debug)
		printf("@@@Sequental forwarding module:%i, type:%i\n",i,modules[i]->m_type);
	      if(debug)
		{
		  std::string s;
		  modules[i]->print(s);
		  printf("weight:\n");
		  if(debug==2)
		  modules[i]->print_weight();
		}
	      modules[i]->forward();
	      if(debug)
		{
		  printf("module output:\n");
		  modules[i]->m_output[0].print_geom();
		  if(debug==2)
		    modules[i]->m_output[0].print();
		}

	      if(i<modules.size()-1)
		modules[i+1]->m_input = modules[i]->m_output;
	    }
	  this->m_output= modules[modules.size()-1]->m_output;
	}
      else
	{
	  this->m_output = this->m_input;
	}
    }

    void print(std::string base)
    {
      using namespace std;
      printf((base+string("Sequential:\n")).c_str());
      for(int i=0;i<modules.size();i++)
	{
	  char tmp2[200];
	  sprintf(tmp2,"%i-->",i);
	  string tmp=base + string(tmp2);
	  modules[i]->print(tmp);
	};
    };
    
  };


  class t7net_concat_table:public t7net_container
  {

  public:

    int input_elements()
    {
      return 1;
    }
    int output_elements()
    {
      return modules.size(); 
    }

    ~t7net_concat_table()
    {
      for(int i=0;i<modules.size();i++)
	delete modules[i];
    }


    void forward(int debug=0)
    {
      m_output.resize(modules.size());
      for(int i=0;i<modules.size();i++)
	{
	  modules[i]->m_input = m_input;
	  modules[i]->forward();
	}
      m_output.clear();
      for(int i=0;i<modules.size();i++)
	m_output.insert(m_output.end(),modules[i]->m_output.begin(),modules[i]->m_output.end());
    }
    void print(std::string base)
    {
      using namespace std;
      printf((base+string("Concat Table:\n")).c_str());
      for(int i=0;i<modules.size();i++)
	{
	  char tmp2[200];
	  sprintf(tmp2,"%i-->",i);
	  string tmp=base + string(tmp2);
	  modules[i]->print(tmp);
	};
    };

  };

  
  class t7net_Conv2d:public t7net_base
  {
  public:
    Tensor m_weight;
    Tensor m_bias;
    //kernel sizes
    int m_kW;
    int m_kH;
    //padding 
    int m_padW;
    int m_padH;
    //stride
    int m_strideW;
    int m_strideH;
    //number of input and output channels
    int m_ichannels;
    int m_ochannels; 
    //cudnn-related descriptors
    cudnnConvolutionDescriptor_t m_conv_desc;
    cudnnFilterDescriptor_t m_filter_desc;
    int m_convAlgo;
    
    //empty
    t7net_Conv2d()
    {
      m_type = Conv2;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_Conv2d()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
      m_weight.release();
      m_bias.release();

      checkCudaErrors(cudaFree(m_conv_desc));
      checkCudaErrors(cudaFree(m_filter_desc));

    }

    void init(int inp,int outp,int kw,int kh,int stridew,int strideh, int padw=0,int padh=0)
    {
      m_type=Conv2;
      m_ichannels=inp;
      m_ochannels=outp;
      m_kW=kw;
      m_kH=kh;
      m_padW=padw;
      m_padH=padh;
      m_strideW=stridew;
      m_strideH=strideh;
     
      m_convAlgo=-1 ;
      checkCUDNN( cudnnCreateFilterDescriptor(&m_filter_desc) );
      checkCUDNN( cudnnCreateConvolutionDescriptor(&m_conv_desc) );

      m_weight = Tensor(m_ochannels,m_ichannels,m_kH,m_kW);
      m_weight.fill(2);
      m_bias = Tensor(1,m_ochannels,1,1);
      m_bias.fill(0);
    }


    void forward(int debug=0)
    {

      //the following part is taken form CUDNN 5.1 examples
      cudnnTensorFormat_t tensor_format = CUDNN_TENSOR_NCHW;

      int n,c,h,w;
      const int tensorDims = 4;
      int tensorOuputDimA[tensorDims] = {n,c,h,w};
      const int filterDimA[tensorDims] = {m_ochannels, m_ichannels, 
					  m_kW, m_kH};
                                       
      checkCUDNN( cudnnSetFilterNdDescriptor(m_filter_desc,
					     Tensor::m_dataType,
					     tensor_format,
					     tensorDims,
					     filterDimA) );
 
      //convolution dimension -- this is 2d convolution!
      const int convDims = 2;
      int padA[convDims] = {m_padW,m_padH};
      int filterStrideA[convDims] = {m_strideW,m_strideH};
      int upscaleA[convDims] = {1,1};
      cudnnDataType_t  convDataType = Tensor::m_dataType;
      convDataType = CUDNN_DATA_FLOAT; 
      checkCUDNN( cudnnSetConvolutionNdDescriptor(m_conv_desc,
						  convDims,
						  padA,
						  filterStrideA,
						  upscaleA,
						  CUDNN_CROSS_CORRELATION,//or CUDNN_CROSS_CORRELATION
						  convDataType) );

      checkCUDNN( cudnnGetConvolutionNdForwardOutputDim(m_conv_desc,
							m_input[0].desc,
							m_filter_desc,
							tensorDims,
							tensorOuputDimA) );
      n = tensorOuputDimA[0]; c = tensorOuputDimA[1];
      h = tensorOuputDimA[2]; w = tensorOuputDimA[3];

      size_t mem_limit = m_input[0].n*m_input[0].c*m_input[0].h*m_input[0].w*sizeof(float);

      cudnnConvolutionFwdAlgo_t algo;
      m_convAlgo =1;


      if(m_convAlgo<0)
	m_output[0]=Tensor(n,c,h,w);

      
      if (m_convAlgo< 0)
        {
	  const int print=1;
	  
	  // Choose the best according to the preference
	  if(print)
	    std::cout << "Testing cudnnGetConvolutionForwardAlgorithm ...\n";
	  checkCUDNN( cudnnGetConvolutionForwardAlgorithm(Tensor::m_CudnnHandle,
							  m_input[0].desc,
							  m_filter_desc,
							  m_conv_desc,
							  m_output[0].desc,
							  CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
							  mem_limit,
							  &algo
							  ) );

	  m_convAlgo= algo;
	  if(print)
	    {
	      std::cout << "Fastest algorithm is Algo " << algo << "\n";
	      // New way of finding the fastest config
	      // Setup for findFastest call
	      std::cout << "Testing cudnnFindConvolutionForwardAlgorithm ...\n";
	    }
	  int requestedAlgoCount = 5; 
	  int returnedAlgoCount[1];
	  cudnnConvolutionFwdAlgoPerf_t *results = (cudnnConvolutionFwdAlgoPerf_t*)malloc(sizeof(cudnnConvolutionFwdAlgoPerf_t)*requestedAlgoCount);
	  checkCUDNN(cudnnFindConvolutionForwardAlgorithm( Tensor::m_CudnnHandle, 
							   m_input[0].desc,
							   m_filter_desc,
							   m_conv_desc,
							   m_output[0].desc,
							   requestedAlgoCount,
							   returnedAlgoCount,
							   results
							   ) );

	  if(print)
	    {
	      for(int algoIndex = 0; algoIndex < *returnedAlgoCount; ++algoIndex){
		printf("^^^^ %s for Algo %d: %f time requiring %llu memory\n", cudnnGetErrorString(results[algoIndex].status), results[algoIndex].algo, results[algoIndex].time, (unsigned long long)results[algoIndex].memory);
	      }
	    }
	  free(results);
        }
      else
        {
	  algo = (cudnnConvolutionFwdAlgo_t)m_convAlgo;
	  if (algo == CUDNN_CONVOLUTION_FWD_ALGO_FFT)
            {
            }
        }

      m_output[0].release();
      m_output[0]=Tensor(n,c,h,w);
      m_output[0].fill(0);

      
      size_t sizeInBytes=0;
      void* workSpace=NULL;
      checkCUDNN( cudnnGetConvolutionForwardWorkspaceSize(Tensor::m_CudnnHandle,
							  m_input[0].desc,
							  m_filter_desc,
							  m_conv_desc,
							  m_output[0].desc,
							  algo,
							  &sizeInBytes) );
      if (sizeInBytes!=0)
        {
          checkCudaErrors( cudaMalloc(&workSpace,sizeInBytes) );
        }
      float alpha = 1;
      float beta  = 0;
      checkCUDNN( cudnnConvolutionForward(Tensor::m_CudnnHandle,
					  &alpha,
					  m_input[0].desc,
					  m_input[0].data,
					  m_filter_desc,
					  m_weight.data,
					  m_conv_desc,
					  algo,
					  workSpace,
					  sizeInBytes,
					  &beta,
					  m_output[0].desc,
					  m_output[0].data) );


      if (sizeInBytes!=0)
        {
          checkCudaErrors( cudaFree(workSpace) );
        }

      alpha=1;
      beta=1;
      checkCUDNN(cudnnAddTensor(Tensor::m_CudnnHandle,&alpha,m_bias.desc,m_bias.data,&beta,m_output[0].desc,m_output[0].data));
    }







    

    


    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module Conv2d:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);

      binn_object_get_int32(obj,(fname+string("_m_kW")).c_str(),&m_kW);
      binn_object_get_int32(obj,(fname+string("_m_kH")).c_str(),&m_kH);
      binn_object_get_int32(obj,(fname+string("_m_padW")).c_str(),&m_padW);
      binn_object_get_int32(obj,(fname+string("_m_padH")).c_str(),&m_padH);
      binn_object_get_int32(obj,(fname+string("_m_strideW")).c_str(),&m_strideW);
      binn_object_get_int32(obj,(fname+string("_m_strideH")).c_str(),&m_strideH);

      binn_object_get_int32(obj,(fname+string("_m_ichannels")).c_str(),&m_ichannels);
      binn_object_get_int32(obj,(fname+string("_m_ochannels")).c_str(),&m_ochannels);

      init(m_ichannels,m_ochannels,m_kW,m_kH,m_strideW,m_strideH,m_padW,m_padH);
      Tensor::deserialize(fname+string("_m_weight"),obj,m_weight);
      Tensor::deserialize(fname+string("_m_bias"),obj,m_bias);

      m_bias.reshape(1,m_weight.n,1,1); 

    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"Conv2d(%i->%i %ix%i %ix%i)",m_ichannels,m_ochannels,m_kW,m_kH,m_strideW,m_strideH);
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
      printf("WEIGHT:\n");
      if(m_weight.data and m_weight.n>0)
	m_weight.print();
      printf("BIAS:\n");
      m_bias.print();
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }

  };

  class t7net_pool:public t7net_base
  {
  public:
    //kernel sizes
    int m_kW;
    int m_kH;

    //padding 
    int m_padW;
    int m_padH;
    //stride
    int m_strideW;
    int m_strideH;

    //cudnn-related descriptors
    cudnnPoolingDescriptor_t m_pooling_desc;
    cudnnPoolingMode_t m_pool_type;
    
    //empty
    t7net_pool()
    {
      m_type = Pool ;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_pool()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
      checkCudaErrors(cudaFree(m_pooling_desc));
    }
    //pooling
    void init(int kw, int kh,int stridew,int strideh, int padw=0, int padh=0,cudnnPoolingMode_t pool_type=CUDNN_POOLING_MAX)
    {

      checkCUDNN( cudnnCreatePoolingDescriptor(&m_pooling_desc) );
      
      m_kW=kw;
      m_kH=kh;
      m_padW=padw;
      m_padH=padh;
      m_strideW=stridew;
      m_strideH=strideh;
      m_pool_type = pool_type;

      cudnnNanPropagation_t maxpoolingNanOpt=CUDNN_PROPAGATE_NAN;//CUDNN_NOT_PROPAGATE_NAN


      
      m_type = Pool;
      const int poolDims = 2;
      int windowDimA[poolDims] = {m_kW,m_kH};
      int paddingA[poolDims] = {m_padW,m_padH};
      int strideA[poolDims] = {m_strideW,m_strideH};
      if(m_padW<0 || m_padH <0)
	{
	  printf("Error! padding less than zero!\n");
	  exit(-1);
	};
      checkCUDNN( cudnnSetPoolingNdDescriptor(m_pooling_desc,
					      m_pool_type,
					      maxpoolingNanOpt,
					      poolDims,
					      windowDimA,
					      paddingA,
					      strideA ) );
    }


    

    void forward(int debug=0)
    {

      const int tensorDims = 4;
      int tensorOuputDimA[tensorDims] = {1,1,1,1};
      checkCUDNN( cudnnGetPoolingNdForwardOutputDim(m_pooling_desc,
                                                    m_input[0].desc,
                                                    tensorDims,
                                                    tensorOuputDimA) );
      int n,c,h,w;
      n = tensorOuputDimA[0]; c = tensorOuputDimA[1];
      h = tensorOuputDimA[2]; w = tensorOuputDimA[3];


      m_output[0] =Tensor(n,c,h,w);

      
      float  alpha = 1;
      float beta = 0;
      checkCUDNN( cudnnPoolingForward(Tensor::m_CudnnHandle,
				      m_pooling_desc,
				      &alpha,
				      m_input[0].desc,
				      m_input[0].data,
				      &beta,
				      m_output[0].desc,
				      m_output[0].data) );
      checkCudaErrors(cudaDeviceSynchronize());
    }


    


    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module Pool:%s\n",fname.c_str());
      using namespace std;
      
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);

      binn_object_get_int32(obj,(fname+string("_m_kW")).c_str(),&m_kW);
      binn_object_get_int32(obj,(fname+string("_m_kH")).c_str(),&m_kH);
      binn_object_get_int32(obj,(fname+string("_m_padW")).c_str(),&m_padW);
      binn_object_get_int32(obj,(fname+string("_m_padH")).c_str(),&m_padH);
      binn_object_get_int32(obj,(fname+string("_m_strideW")).c_str(),&m_strideW);
      binn_object_get_int32(obj,(fname+string("_m_strideH")).c_str(),&m_strideH);

      int tmp;
      binn_object_get_int32(obj,(fname+string("_m_pool_type")).c_str(),&tmp);


      switch(tmp)
	{
	case 0:
	  m_pool_type= CUDNN_POOLING_MAX;
	  break;
	case 1:
	  m_pool_type = CUDNN_POOLING_AVERAGE_COUNT_INCLUDE_PADDING;
	  break;
	};
      init(m_kW,m_kH,m_strideW,m_strideH,m_padW,m_padH,cudnnPoolingMode_t(m_pool_type));
      
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"Pool(%ix%i %ix%i),mode:%i",m_kW,m_kH,m_strideW,m_strideH,m_pool_type);
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }
  };


  class t7net_linear:public t7net_base
  {
  public:
  
    //self weights -- as in Torch
    Tensor m_weight;
    Tensor m_bias;


    //for LINEAR module
    int m_inp_size;
    int m_outp_size;


    
    //empty
    t7net_linear()
    {
      m_type = Linear;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_linear()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
      m_weight.release();
      m_bias.release();
    }
    void init(int inp_size,int outp_size)
    {
      m_inp_size = inp_size;
      m_outp_size = outp_size;
      m_type = Linear;
      m_weight = Tensor(1,1,outp_size,inp_size);
      m_bias=Tensor(1,1,1,outp_size);
      m_output.resize(1);
      m_output[0]=Tensor(1,1,outp_size,1);
      m_output[0].fill(0);
    }

    void forward(int debug=0)
    {
      m_output[0].release();
      m_output[0]=Tensor(m_input[0].n,1,m_weight.h,1);

      for(int i=0;i<m_input[0].n;i++)
	{
	  Tensor::tensor_to_tensor(m_bias.data,m_output[0].data + m_bias.n*m_bias.c*m_bias.h*m_bias.w*i,m_bias.n*m_bias.c*m_bias.h*m_bias.w*sizeof(float));
	}
      
      //##### CUBLAS MATRIX MULTIPLICATION
      float alpha =1.f;
      float beta=1.f;

      //A(mxk)* B(kx1) + C (mx1)
      int k=m_input[0].h;
      int m=m_output[0].h;
      int n=m_input[0].n;
      cublasSgemm(Tensor::m_CublasHandle,CUBLAS_OP_T,CUBLAS_OP_N,m,n,k,&alpha,m_weight.data,m_weight.w,m_input[0].data,m_input[0].h,&beta,m_output[0].data,m_output[0].h);
    }

    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module Linear:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
      binn_object_get_int32(obj,(fname+string("_m_inp_size")).c_str(),&m_inp_size);
      binn_object_get_int32(obj,(fname+string("_m_outp_size")).c_str(),&m_outp_size);
      init(m_inp_size,m_outp_size);
      Tensor::deserialize(fname+string("_m_weight"),obj,m_weight);
      Tensor::deserialize(fname+string("_m_bias"),obj,m_bias);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"Linear(%i->%i)",m_inp_size,m_outp_size);
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
      printf("Weight:\n");
      if(m_weight.data and m_weight.n>0)
	m_weight.print();
      printf("Bias:\n");
      m_bias.print();
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }
  };


  class t7net_activation:public t7net_base
  {
  public:
    cudnnActivationMode_t m_activation;
    cudnnActivationDescriptor_t m_activation_descriptor;
    //empty
    t7net_activation()
    {
      m_type = Activation;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_activation()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
    }

    void init(cudnnActivationMode_t act)
    {
      m_type = Activation;
      m_activation = act;
      cudnnCreateActivationDescriptor(&m_activation_descriptor);
      cudnnSetActivationDescriptor(m_activation_descriptor,m_activation,CUDNN_PROPAGATE_NAN,0);
    }     
    void forward(int debug=0)
    {
      m_output[0]=m_input[0].clone();
      float alpha=1;
      float beta=0;
      checkCUDNN( cudnnActivationForward(Tensor::m_CudnnHandle,
					 m_activation_descriptor,
					 &alpha,
					 m_input[0].desc,
					 m_input[0].data,
					 &beta,
					 m_output[0].desc,
					 m_output[0].data) );    
    }

    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module Activation:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
      int tmp;
      binn_object_get_int32(obj,(fname+string("_m_activation")).c_str(),&tmp);
      if(tmp==0)
	m_activation= CUDNN_ACTIVATION_RELU;
      init(m_activation);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"Activation:%i",m_activation);
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }
  };
  
  class t7net_padding:public t7net_base
  {
  public:
     int m_padW;
    //number of input and output channels
    int m_ichannels;
    int m_ochannels;
    t7net_padding()
    {
      m_type = Padding;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_padding()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
    }
    void init(int dim, int pad)
    {
      this->m_ichannels = dim;
      this->m_padW = pad;
      m_type = Padding;
    }

    void forward(int debug=0)
    {
      int dim=m_ichannels;
      int pad=m_padW;
      
      m_output.resize(1);

      if(pad==0)
	{
	  m_output=m_input;
	  return;
	};

      
      //      printf("dim:%i pad:%i\n",dim,pad);
      if(dim==1)
	{
	  m_output.resize(1);
	  m_output[0]=Tensor(m_input[0].n,m_input[0].c+pad,m_input[0].h,m_input[0].w);
	  m_output[0].fill(0);

	  Tensor_view src = m_input[0].view();
	  Tensor_view dst(0,m_input[0].n,0,m_input[0].c,0,m_input[0].h,0,m_input[0].w);

	  Tensor::copy(dst,m_output[0],src,m_input[0]);
	}
      else
	{
	  printf("unknown dim in padding!!! bailing out!\n");
	  exit(-1);
	}


    }


    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module Padding:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
      binn_object_get_int32(obj,(fname+string("_m_ichannels")).c_str(),&m_ichannels);
      binn_object_get_int32(obj,(fname+string("_m_padW")).c_str(),&m_padW);
      init(m_ichannels,m_padW);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"Padding(dim=%i pad=%i)",m_ichannels,m_padW);
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }
  };


  class t7net_view:public t7net_base
  {
  public:
    int m_inp_size;
  
    t7net_view()
    {
      m_type = View;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_view()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
    }

    void init()
    {
      m_type = View;
      
    }
    void forward(int debug=0)
    {
      m_output =m_input; 
      m_output[0].reshape(m_input[0].n,1,m_inp_size,1);
    }
    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module View:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
      binn_object_get_int32(obj,(fname+string("_m_inp_size")).c_str(),&m_inp_size);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;

      strcpy(tmp,"View");
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
    }

  };

  
  class t7net_dropout:public t7net_base
  {
  public:
  
    float m_p;
    
    //empty
    t7net_dropout()
    {
      m_type = Dropout;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_dropout()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
    }



    void init()
    {
      m_type = Dropout;
    }

    void forward(int debug=0)
    {
      m_output=m_input;
      float param=m_p;
      param=1.-param;
      cuda_functions::apply_transform(m_output[0].data,m_output[0].n*m_output[0].c*m_output[0].h*m_output[0].w,cuda_functions::multiply,param);
    }
    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module Dropout:%s\n",fname.c_str());
      using namespace std;
      
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);

      Tensor m_bias;
      Tensor::deserialize(fname+string("_m_bias"),obj,m_bias);
      cv::Mat tmp;
      Tensor::mat_from_tensor(tmp,m_bias,0,0);
      float param=tmp.at<float>(0,0);
      m_p=param;
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      strcpy(tmp,"Dropout");
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
    }
  };

  class t7net_addtable:public t7net_base
  {
  public:
    t7net_addtable()
    {
      m_type = AddTable;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_addtable()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
    }
    void init()
    {
      m_type = AddTable;
    }
    void forward(int debug=0)
    {
      m_output.resize(1);
      float alpha=1,beta=1;
      m_output[0]=m_input[0].clone();
      for(int i=1;i<m_input.size();i++)
	{
	  assert(m_input[i].n==m_output[0].n and m_input[i].c==m_output[0].c and m_input[i].h==m_output[0].h and m_input[i].w==m_output[0].w);
	  checkCUDNN(cudnnAddTensor(Tensor::m_CudnnHandle,&alpha,m_input[i].desc,m_input[i].data,&beta,m_output[0].desc,m_output[0].data));
	}
    }


    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module AddTable:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      strcpy(tmp,"AddTable");
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
    }
  };


  class t7net_spatialbatchnorm:public t7net_base
  {
  public:
  
    Tensor m_mean;
    Tensor m_std;
    int m_affine;

    Tensor m_weight;
    Tensor m_bias;
    t7net_spatialbatchnorm()
    {
      m_type = SpatialBatchNorm;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_spatialbatchnorm()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
      m_weight.release();
      m_bias.release();
    }

    void init()
    {
      m_type = SpatialBatchNorm;
    }

    void forward(int debug=0)
    {
      
      int nBatch = m_input[0].n;
      int nFeature = m_input[0].c;
      int iH = m_input[0].h;
      int iW = m_input[0].w;

      Tensor mean=m_mean;
      Tensor std=m_std;
      int affine=m_affine;

      mean.reshape(1,m_input[0].c,1,1);
      std.reshape(1,m_input[0].c,1,1);

      float alpha=-1;
      float beta=1;

      if(m_output[0].n!=m_input[0].n or m_output[0].c!=m_input[0].c or m_output[0].h!=m_input[0].h or m_output[0].w!=m_input[0].w)
	m_output[0]=m_input[0].clone();
      else
	Tensor::copy(m_output[0].view(),m_output[0],m_input[0].view(),m_input[0]);
      checkCUDNN(cudnnAddTensor(Tensor::m_CudnnHandle,&alpha,mean.desc,mean.data,&beta,m_output[0].desc,m_output[0].data));
      beta=1;
      float beta2=0;


      cudnnOpTensorDescriptor_t opTensorDesc;
      cudnnCreateOpTensorDescriptor(&opTensorDesc);
      cudnnSetOpTensorDescriptor(opTensorDesc,CUDNN_OP_TENSOR_MUL,CUDNN_DATA_FLOAT,CUDNN_PROPAGATE_NAN );


      
      alpha =1 ;
      beta =1 ;
      checkCUDNN(cudnnOpTensor(Tensor::m_CudnnHandle,opTensorDesc,&alpha,m_output[0].desc,m_output[0].data,&beta,std.desc,std.data,&beta2,m_output[0].desc,m_output[0].data));

      alpha=1;
      beta=1;
      beta2=0;
      m_weight.reshape(1,m_input[0].c,1,1);
      m_bias.reshape(1,m_input[0].c,1,1);

      checkCUDNN(cudnnOpTensor(Tensor::m_CudnnHandle,opTensorDesc,&alpha,m_output[0].desc,m_output[0].data,&beta,m_weight.desc,m_weight.data,&beta2,m_output[0].desc,m_output[0].data));
      alpha=1;
      beta=1;
      checkCUDNN(cudnnAddTensor(Tensor::m_CudnnHandle,&alpha,m_bias.desc,m_bias.data,&beta,m_output[0].desc,m_output[0].data));
      checkCUDNN(cudnnDestroyOpTensorDescriptor(opTensorDesc));

    }
   
    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module SpatialBatchNorm:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
      init();
      Tensor::deserialize(fname+string("_m_running_mean"),obj,m_mean);
      Tensor::deserialize(fname+string("_m_running_std"),obj,m_std);
      binn_object_get_int32(obj,(fname+string("_m_affine")).c_str(),&m_affine);
      Tensor::deserialize(fname+string("_m_weight"),obj,m_weight);
      Tensor::deserialize(fname+string("_m_bias"),obj,m_bias);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;

      strcpy(tmp,"SPATIALBATCHNORM");
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
      printf("WEIGHT:\n");
      if(m_weight.data and m_weight.n>0)
	m_weight.print();
      printf("BIAS:\n");
      m_bias.print();
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }
  };


  class t7net_spatialzeropad:public t7net_base
  {
  public:
  

    //padding 
    int m_padL;
    int m_padR;
    int m_padT;
    int m_padB;





    //empty
    t7net_spatialzeropad()
    {
      m_type = SpatialZeroPad;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_spatialzeropad()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
    }
    void init(int pad_l,int pad_r,int pad_t,int pad_b)
    {
      m_type = SpatialZeroPad;
      m_padL=pad_l;
      m_padR=pad_r;
      m_padT=pad_t;
      m_padB=pad_b;
    }

    void forward(int debug=0)
    {
      m_output.resize(1);
      m_output.resize(1);
      m_output[0]=Tensor(m_input[0].n,m_input[0].c,m_input[0].h+m_padT+m_padB,m_input[0].w+m_padL+m_padR);
      m_output[0].fill(0);
      
      Tensor_view src = m_input[0].view();
      if(m_padT<0)
	src.h1+=m_padT;
      if(m_padB<0)
	src.h2-=m_padB;


      if(m_padL<0)
	src.w1+=m_padL;
      if(m_padR<0)
	src.w2-=m_padR;
	  
      Tensor_view dst(0,m_output[0].n,0,m_output[0].c,0,m_output[0].h,0,m_output[0].w);
	    

      if(src.h1>0)
	dst.h1=0;
      else//src.h1==0
	dst.h1=m_padT;
	
      if(src.w1>0)
	dst.w1=0;
      else//src.h1==0
	dst.w1=m_padL;

      dst.h2=dst.h1+src.h2-src.h1;
      dst.w2=dst.w1+src.w2-src.w1;
      Tensor::copy(dst,m_output[0],src,m_input[0]);
    }

    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module SpatialZeropad:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
      binn_object_get_int32(obj,(fname+string("_m_padL")).c_str(),&m_padL);
      binn_object_get_int32(obj,(fname+string("_m_padR")).c_str(),&m_padR);
      binn_object_get_int32(obj,(fname+string("_m_padT")).c_str(),&m_padT);
      binn_object_get_int32(obj,(fname+string("_m_padB")).c_str(),&m_padB);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"Padding(t %i b %i l %i r %i)",m_padT,m_padB,m_padL,m_padR);
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }
  };




  class t7net_spatialcontrast:public t7net_base
  {
  public:
  
    Tensor m_weight;
    Tensor m_bias;



    //kernel sizes
    int m_kW;
    int m_kH;

    //padding 
    int m_padW;
    int m_padH;
    //stride
    int m_strideW;
    int m_strideH;


    //local contrast normalization threshold
    float m_lcn_thresh;




    t7net_sequential *estimator;

    float kernel_sum;
    Tensor m_ones,m_coef;
    int m_input_planes;
    
    
    //empty
    t7net_spatialcontrast()
    {
      estimator =NULL;
      m_type = SpatialContrast;
      m_input.resize(1);
      m_output.resize(1);
    }
    ~t7net_spatialcontrast()
    {
      if(estimator)
	delete estimator;
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
      m_weight.release();
      m_bias.release();
    }



  
    void init(int kernel_norm=0)
    {
      m_type = SpatialContrast;
      estimator = new t7net_sequential();
      float ksum = Tensor::sum(m_weight);
      if(kernel_norm!=0)
	Tensor::apply_transform(m_weight,cuda_functions::multiply,1./ksum);


      t7net_Conv2d *mod1=new t7net_Conv2d();
      mod1->init(m_input_planes,m_input_planes,m_weight.w,m_weight.h,1,1);

      mod1->m_weight.print_geom();
      for(int i=0;i<m_input_planes;i++)
	{
	  for(int j=0;j<m_input_planes;j++)
	    {
	      Tensor_view src=m_weight.view();
	      Tensor_view dst(i,i+1,j,j+1,0,m_weight.h,0,m_weight.w);
	      Tensor::copy(dst,mod1->m_weight,src,m_weight);
	    }
	}

      
      mod1->m_bias.fill(0);
      mod1->m_weight.print();
      kernel_sum=ksum;


      t7net_spatialzeropad *mod2=new t7net_spatialzeropad();
      int padH=m_weight.h/2;
      int padW=padH;
      mod2->init(padW,padW,padH,padH);
      estimator->add(mod2);
      estimator->add(mod1);
    }
    void forward(int debug=0)
    {

      if(Tensor::compare_dimensions(m_ones,m_input[0])==false)
	{
	  m_ones.release();
	  m_ones=m_input[0].clone();
	}
      m_ones.fill(1);

      
      if(Tensor::compare_dimensions(estimator->m_input[0],m_ones)==false)
	{
	  estimator->m_input[0].release();
	  estimator->m_input[0]=m_ones.clone();
	}
      Tensor::copy(estimator->m_input[0],m_ones);
      estimator->forward();

      Tensor::copy(m_ones,estimator->m_output[0]);

      if(Tensor::compare_dimensions(m_coef,estimator->m_output[0])==false)
	{
	  m_coef=estimator->m_output[0].clone();
	}
      else
	Tensor::copy(m_coef,estimator->m_output[0]);
      Tensor localsums;
      Tensor::copy(estimator->m_input[0],m_input[0]);
      estimator->forward();
      localsums = estimator->m_output[0];

      Tensor::divide(localsums,m_coef);
      Tensor::subtract(m_input[0],m_coef);
    

      //divisive norm start
      Tensor::copy(estimator->m_input[0],m_coef);
      Tensor::apply_transform(estimator->m_input[0],cuda_functions::sqr);
      estimator->forward();
      Tensor::apply_transform(estimator->m_output[0],cuda_functions::sqrt);
      Tensor::divide(estimator->m_output[0],m_ones);
      Tensor::apply_transform(m_ones,cuda_functions::threshold,m_lcn_thresh);
      Tensor::divide(m_coef,m_ones);
      m_output[0]=m_ones;
      
    }


    


    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module SpatialContrastNorm:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
      binn_object_get_int32(obj,(fname+string("_m_input_planes")).c_str(),&m_input_planes);
      binn_object_get_int32(obj,(fname+string("_m_kW")).c_str(),&m_kW);
      binn_object_get_int32(obj,(fname+string("_m_kH")).c_str(),&m_kH);
      binn_object_get_int32(obj,(fname+string("_m_padW")).c_str(),&m_padW);
      binn_object_get_int32(obj,(fname+string("_m_padH")).c_str(),&m_padH);
      binn_object_get_int32(obj,(fname+string("_m_strideW")).c_str(),&m_strideW);
      binn_object_get_int32(obj,(fname+string("_m_strideH")).c_str(),&m_strideH);
      binn_object_get_float(obj,(fname+string("_m_lcn_thresh")).c_str(),&m_lcn_thresh);
      Tensor::deserialize(fname+string("_m_weight"),obj,m_weight);
      Tensor::deserialize(fname+string("_m_bias"),obj,m_bias);
     init();
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"SpatialContrast(%i,%ix%i)",m_input_planes,m_weight.h,m_weight.w);
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
    void print_weight()
    {
      printf("Weight:\n");
      if(m_weight.data and m_weight.n>0)
	m_weight.print();
      printf("Bias:\n");
      m_bias.print();
    }

    float control_sum(Tensor &t)
    {
      return Tensor::sum(t); 
    }
  };


  class t7net_identity:public t7net_base
  {
  public:
    t7net_identity()
    {
      m_type = Identity;
    }
    ~t7net_identity()
    {
      for(int i=0;i<m_input.size();i++)
	{
	  m_input[i].release();
	}
      for(int i=0;i<m_output.size();i++)
	{
	  m_output[i].release();
	}
    }
    void init()
    {
      m_type = Identity;
    }

    void forward(int debug=0)
    {
      m_output=m_input;
    }

    void deserialize(std::string fname, binn* obj)
    {
      printf("Reading module Identity:%s\n",fname.c_str());
      using namespace std;
      binn_object_get_int32(obj,(fname+string("_m_type")).c_str(),&m_type);
    }

    void print(std::string f)
    {
      char tmp[200];
      std:: string base = f;
      sprintf(tmp,"Identity()");
      printf((base+std::string(tmp)).c_str());
      printf("\n");
    }
  };



  



  

 
  void deserialize(std::string fname,void* net, binn* obj);


};


#endif
