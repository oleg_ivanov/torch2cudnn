#include "t7net.hh"
#include <cv.h>
using namespace cv;
using namespace std;

int main()
{

  using namespace t7net;
  int device=0;
  Tensor::init_handles(device);







  

  binn *obj=binn_object();
  string fname="test.t7net";
  utils::load_binn(fname,obj);
  t7net_sequential net;
  deserialize("test_net",(void*)&net,obj);
  binn_free(obj);

  printf("### GOT NET:###\n");
  net.print(string(""));

  
  //test matrix
  Mat tmp(24,24,CV_32FC1);
  int counter=0;
  for(int i=0;i<tmp.cols;i++)
    for(int j=0;j<tmp.rows;j++)
      {
	tmp.at<float>(j,i)=counter++;
      }

  Tensor input(1,1,24,24);
  Tensor::mat_to_tensor(tmp,input,0);
  printf("### INPUT: ###\n");
  input.print();

  net.m_input[0]=input;
  net.forward(1);
  Tensor output=net.m_output[0];
  printf("### OUTPUT: ###\n");
  output.print();


 
  return 0;
}
