require 'cunn'
require 'image'
require 'opencv24'
dofile '../torch_converter/convert.lua'


function add_resnet(net,inchannels,outchannels,kx,ky)

   local stride = 1 
   local net1=nn.Sequential()

   local padx=(kx-1)/2
   local pady=(ky-1)/2

   local iter=1

   for i=1,iter do
           if(i==1) then
                   net1:add(nn.SpatialConvolution(inchannels,outchannels,kx,ky,1,1,padx,pady))
           else
                   net1:add(nn.SpatialConvolution(outchannels,outchannels,kx,ky,1,1,padx,pady))
           end
   net1:add(nn.ReLU(true))
   end

   local skip
   if stride > 1 then
      -- optional downsampling
      skip = nn.SpatialAveragePooling(1, 1, stride,stride)
   end
   if outchannels>= inchannels then
      -- optional padding
      local psize=outchannels - inchannels
      print("padding size:",psize)
      skip = nn.Padding(1, psize, 3)
   elseif outchannels< inchannels then
      -- optional narrow, ugh.
      skip = nn.Narrow(2, 1, outchannels)
      -- NOTE this BREAKS with non-batch inputs!!
   end
   local tabular=nn.ConcatTable()
   tabular:add(net1)
   tabular:add(skip)
   net:add(tabular)
   net:add(nn.CAddTable())
   net:add(nn.ReLU(true))

   return net
   
end




channels=1
img_size={24,24}
test_batch=torch.Tensor(1,channels,img_size[1],img_size[2])



---#### Typical net definition for NORB-like data
m=nn.Sequential()
kernel=image.gaussian(9,9)
m:add(nn.SpatialContrastiveNormalization(channels,kernel))
m:add(nn.SpatialConvolution(channels,36,3,3))
m:add(nn.ReLU(true))
m:add(nn.SpatialMaxPooling(2,2,2,2))
m:add(nn.SpatialConvolution(36,36,3,3))
m:add(nn.SpatialBatchNormalization(36))
--ResNet-like
m=add_resnet(m,36,36,3,3)
m:add(nn.SpatialMaxPooling(2,2,2,2))

out_batch=m:forward(test_batch)
view_size=out_batch:size(2)*out_batch:size(3)*out_batch:size(4)

m:add(nn.View(view_size))
m:add(nn.Linear(view_size,view_size))
m:add(nn.ReLU(true))
--#################

-- test input
inp=test_batch
counter=0
for i=1,inp:size(2) do
   for j=1,inp:size(3) do
      for k=1,inp:size(4) do
	 inp[{{},i,j,k}]=counter
	 counter=counter+1
      end
   end
end


torch.save("test.net",m)



--serialization with binn library
opencv24.create_binn_object()
serialize(m,"test_net")
opencv24.save_binn_object("test.t7net")
opencv24.free_binn_object()
print("### INPUT ###")
print(inp)
outp=m:forward(inp)
print("### OUTPUT ###")
print(outp)



